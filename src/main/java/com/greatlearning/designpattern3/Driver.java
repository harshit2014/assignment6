package com.greatlearning.designpattern3;


public class Driver {

	public static void main(String[] args) {

		CurrencyConvertor currencyConvertor = getConverter("GBP");
		
		double inrAmt = currencyConvertor.convertToINR(500);
		System.out.println("500 GBP to INR amount:"+ inrAmt);

		currencyConvertor = getConverter("DOLLAR");
		
		inrAmt = currencyConvertor.convertToINR(700);
		System.out.println("700 Dollar to INR Amount" + inrAmt);
	}
	
	public static CurrencyConvertor getConverter(String currency) {
		switch (currency) {
		case "GBP":
			return new GBPConverter();
		case "DOLLAR":
			return new DollarConvertor();
	}
		return null;
}
}
