package com.greatlearning.designpattern3;

public interface CurrencyConvertor {

	abstract double convertToINR(double amount);
}
