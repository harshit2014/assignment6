package com.greatlearning.designpattern1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JdbcConnection {

	private static final String dbUrl = "jdbc:mysql://localhost:3306/database";
	private static final String userName = "admin";
	private static final String password = "pass@123";
	private static Connection instance;
	
	private JdbcConnection() {}
	
	
	public static Connection getConnection() throws SQLException
    {
                if (instance == null){
                	synchronized(JdbcConnection.class){
                		if(instance ==null){
							instance = DriverManager.getConnection(dbUrl, userName, password);
						}

					}
				}
				return instance;
					
    }
}
