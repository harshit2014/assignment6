package com.greatlearning.designpattern1;

import java.sql.Connection;
import java.sql.SQLException;

public class Driver {
	
	
	public static void main(String[] args) throws SQLException {


		Connection jdbcConnection1 = JdbcConnection.getConnection();
		Connection jdbcConnection2 = JdbcConnection.getConnection();
		System.out.println("singleton? "+ (jdbcConnection1.hashCode()==jdbcConnection2.hashCode()));
				
	}
		
		
		

}
