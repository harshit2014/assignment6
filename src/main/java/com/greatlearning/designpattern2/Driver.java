package com.greatlearning.designpattern2;

public class Driver {

	public static void main(String[] args) {

		BankAccount bankAccount1 = new BankAccountBuilder("201024572", "savings",
				"Mainpuri", "415000")
				.setAtmTransactions("allowed")
				.setEmiSchedule("biweekly")
				.buildBankAccount();
		
	}
}
